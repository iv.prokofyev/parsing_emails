#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import imaplib
import email
import base64
import re
import telebot
import sys
from entities import LOGIN, PASSWORD

TOKEN = '873038856:AAFfoShGv3EWu1D8eGSMn8DazafNY2ElYDY'
CHANNEL_NAME = '@parsing_email'
bot = telebot.TeleBot(TOKEN, threaded=False)

need_sender = ['service@spp-msk.ru', 'info@spp-msk.ru', 'hghost@mail.ru']

dict_result_parsing = {'number_flight_to_svo': '',
                       'date_flight_to_svo': '',
                       'airport_from': '',
                       'airport_transit': '',
                       'airport_to': '',
                       'count_postal_bags': '',
                       'weight_consignment': ''}

try:
    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login(LOGIN, PASSWORD)
    mail.select('INBOX')  # Подключаемся к папке "входящие"
except Exception as error:
    bot.send_message(CHANNEL_NAME, 'Ошибка соединения с сервером Google: {}'.format(error))
    sys.exit(1)

_, all_uid_emails_inbox = mail.uid('search', None, "ALL")  # Выполняет поиск, возврат typ и UID всех писем в Inbox.

try:
    first_email_uid = all_uid_emails_inbox[0].split()[0]
except:
    sys.exit(0)

_, main_content_email = mail.uid('fetch', first_email_uid, '(RFC822)')  # Получаем тело письма (RFC822) для данного ID
raw_email = main_content_email[0][1]  # Сокращаем тело письма до теста сообщения и отправителя
email_message = email.message_from_bytes(raw_email)
email_sender = email_message['From']
email_sender = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", email_sender)[0]

if email_sender not in str(need_sender):
    bot.send_message(CHANNEL_NAME, 'Поступило письмо от неавторизованного отправителя: {}'.format(email_sender))
    mail.store(b'1', '+FLAGS', '\\Deleted')
    mail.expunge()
    sys.exit(1)

if email_message.is_multipart():
    for part in email_message.walk():
        if part.get_content_type() == 'text/plain':
            try:
                body_email_text = base64.b64decode(part.get_payload()).decode('utf-8')
            except:
                mail.store(b'1', '+FLAGS', '\\Deleted')
                mail.expunge()
                print('Емейл удален. Содержит вложения не для парсинга.')
                sys.exit(1)

prepare_shipmens = re.findall('SU.*?леги|SU.*?кг', body_email_text.replace('\n', ' '))  # Удаляем перенос строки и лишний текст

shipments = []

for i in prepare_shipmens:
    shipments.extend(re.findall('SU.*?кг', i))

for shipment in shipments:
    dict_result_parsing.clear()
    number_flight_to_svo = re.findall('SU\d{4}', shipment)
    if not number_flight_to_svo:
        bot.send_message(CHANNEL_NAME, 'Недостаточно данных для БД. Номер рейса отсутствует.')
        sys.exit(1)
    number_flight_to_svo = ''.join(number_flight_to_svo)
    dict_result_parsing['number_flight_to_svo'] = number_flight_to_svo
    date_flight_to_svo = re.findall('\d\d.\d\d.\d{4}|\d{2}JUN|\d{2} JUN', shipment)
    if not date_flight_to_svo:
        bot.send_message(CHANNEL_NAME, 'Недостаточно данных для БД. Дата рейса отсутствует.')
        sys.exit(1)
    date_flight_to_svo = ''.join(date_flight_to_svo)
    try:
        date_flight_to_svo = date_flight_to_svo.replace(' JUN', ' 06 2019')
        date_flight_to_svo = date_flight_to_svo.replace('JUN', ' 06 2019')
    except:
        bot.send_message(CHANNEL_NAME, 'Недостаточно данных для БД. Формат даты неверный.')
    date_flight_to_svo = re.sub('[/, -]', '.', date_flight_to_svo)
    dict_result_parsing['date_flight_to_svo'] = date_flight_to_svo
    airports = re.findall('[aA-zZ]{3}.[aA-zZ]{3}.[aA-zZ]{3}', shipment)
    if not airports:
        bot.send_message(CHANNEL_NAME, 'Недостаточно данных для БД. Аэропорты отсутствуют.')
        sys.exit(1)
    airports = ''.join(airports)                                         # переводим список в стринг
    airports = re.findall('[aA-zZ]{3}', airports)
    dict_result_parsing['airport_from'] = airports[0].upper()
    dict_result_parsing['airport_transit'] = airports[1].upper()
    dict_result_parsing['airport_to'] = airports[2].upper()
    count_postal_bags = re.findall('кол.*?ме', shipment)
    count_postal_bags = ''.join(count_postal_bags)
    count_postal_bags = re.findall('\d+', count_postal_bags)
    if not count_postal_bags:
        bot.send_message(CHANNEL_NAME, 'Недостаточно данных для БД. Количество мест отсутствует.')
        sys.exit(1)
    count_postal_bags = ''.join(count_postal_bags)
    dict_result_parsing['count_postal_bags'] = count_postal_bags
    weight_consignment = re.findall('мес.*?кг', shipment)
    weight_consignment = ''.join(weight_consignment)
    weight_consignment = re.sub(',', '.', weight_consignment)
    weight_consignment = re.findall('\d+(?:\.\d+)?', weight_consignment)
    if not weight_consignment:
        bot.send_message(CHANNEL_NAME, 'Недостаточно данных для БД. Вес партии отсутствует.')
        sys.exit(1)
    weight_consignment = ''.join(weight_consignment)
    dict_result_parsing['weight_consignment'] = weight_consignment

    print(dict_result_parsing)
    bot.send_message(CHANNEL_NAME, 'Данные для бронирования собраны успешно: {}'.format('-'.join(dict_result_parsing.values())))


# mail.store(b'1', '+FLAGS', '\\Deleted')
# mail.expunge()






